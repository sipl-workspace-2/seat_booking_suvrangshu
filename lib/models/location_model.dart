import 'package:flutter/material.dart';
import 'package:seat_booking/models/location_image.dart';
import 'package:seat_booking/models/seat_model.dart';

class LocationModel {
  late int id;
  late String name;
  late List<String> category;
  late String boardingLocation;
  late String describtion;
  late Image image;
  late Image imageLogo;
  List<LocationImages> locationImageList = List<LocationImages>.empty(growable: true);
  List<dynamic>? seats = List<dynamic>.empty(growable: true);

  LocationModel({
    required this.id,
    required this.category,
    required this.name,
    required this.boardingLocation,
    required this.describtion,
    required this.image,
    required this.imageLogo,
    required this.locationImageList,
    this.seats,
  });
  // LocationModel.fromMap(Map<String, dynamic> map) {
  //   seats = map['seats'].map((seats) => SeatModel.fromMap(seats)).toList();}
}
