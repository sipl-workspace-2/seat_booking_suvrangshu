import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:seat_booking/constants/constants.dart';

import 'package:seat_booking/screens/booking/components/custom_app_bar.dart';
import 'package:seat_booking/screens/booking/components/date_selector.dart';
import 'package:seat_booking/screens/booking/components/location_text.dart';
import 'package:seat_booking/screens/booking/components/pay_button.dart';
import 'package:seat_booking/screens/booking/components/time_selector.dart';
import '../../models/location_model.dart';

class BookingScreen extends StatefulWidget {
  final String locationName;
  final LocationModel locationModel;

  BookingScreen({required this.locationName, required this.locationModel});

  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
  int? seatSelected;
  int? seatSelected2;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: [
            //app bar
            CustomAppBar(widget.locationName),

            //date selector
            DateSelector(),

            //Time selector
            TimeSelector(),

            //Location and theatre
            LocationText(),

            // Seat selector
            // SeatSelector(),
            Expanded(
              flex: 35,
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      DelayedDisplay(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.6,
                          margin: EdgeInsets.fromLTRB(64, 16, 64, 16),
                          child: Stack(
                            children: [
                              Column(
                                children: [
                                  Spacer(),
                                  Container(
                                    height: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      border:
                                          Border.all(color: primary, width: 2),
                                    ),
                                  ),
                                  Spacer(flex: 2),
                                  Container(
                                    height: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      border:
                                          Border.all(color: primary, width: 2),
                                    ),
                                  ),
                                  Spacer(),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(40),
                                  ),
                                  border: Border.all(color: primary, width: 2),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(40),
                                  ),
                                  child: GridView.count(
                                    crossAxisCount: 4,
                                    mainAxisSpacing: 1,
                                    crossAxisSpacing: 0,
                                    children: widget.locationModel.seats!
                                        .asMap()
                                        .map((index, element) {
                                          return MapEntry(
                                            index,
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  seatSelected = index;
                                                  print(seatSelected);
                                                });
                                              },
                                              child: Container(
                                                margin: EdgeInsets.all(16),
                                                child: Image.asset(
                                                  seatSelected == index
                                                      ? 'assets/image/seats/seat_3.jpg'
                                                      : element.available!
                                                          ? 'assets/image/seats/seat_1.jpg'
                                                          : 'assets/image/seats/seat_2.jpg',
                                                  width: 28,
                                                ),
                                              ),
                                            ),
                                          );
                                        })
                                        .values
                                        .toList(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 32),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/image/seats/seat_1.jpg',
                              width: 28,
                            ),
                            SizedBox(width: 8),
                            Text(
                              'available',
                              style: TextStyle(
                                color: primary,
                              ),
                            ),
                            SizedBox(width: 12),
                            Image.asset(
                              'assets/image/seats/seat_2.jpg',
                              width: 28,
                            ),
                            SizedBox(width: 8),
                            Text(
                              'booked',
                              style: TextStyle(
                                color: primary,
                              ),
                            ),
                            SizedBox(width: 12),
                            Image.asset(
                              'assets/image/seats/seat_3.jpg',
                              width: 28,
                            ),
                            SizedBox(width: 8),
                            Text(
                              'select',
                              style: TextStyle(
                                color: primary,
                              ),
                            ),
                          ],
                        ),
                      ),
                      // DelayedDisplay(
                      //   child: Container(
                      //     margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                      //     width: MediaQuery.of(context).size.width,
                      //     // child: ElevatedButton(
                      //     //   onPressed: () {},
                      //     //   style: ButtonStyle(
                      //     //     backgroundColor:
                      //     //         MaterialStateProperty.resolveWith<Color>(
                      //     //       (Set<MaterialState> states) {
                      //     //         if (states.contains(MaterialState.pressed))
                      //     //           return primary;
                      //     //         return secondary;
                      //     //       },
                      //     //     ),
                      //     //   ),
                      //     //   // child: Padding(
                      //     //   //   padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
                      //     //   //   child: Text(
                      //     //   //     'Proceed to payment',
                      //     //   //     style: TextStyle(color: Colors.white),
                      //     //   //   ),
                      //     //   // ),
                      //     // ),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),

            // Pay button and seat categories
            // Busseats2(),
            PayButton(),
          ],
        ),
      ),
    );
  }
}
