import 'package:flutter/material.dart';
import 'package:seat_booking/constants/constants.dart';
import 'package:seat_booking/screens/booking/booking_screen.dart';

import '../../../models/location_model.dart';

class BuyButton extends StatefulWidget {
  final String movieName;
  final LocationModel movie;

  BuyButton(this.movieName, this.movie);

  @override
  _BuyButtonState createState() => _BuyButtonState();
}

class _BuyButtonState extends State<BuyButton> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Positioned(
      bottom: 0,
      child: Container(
        width: size.width * 0.9,
        height: size.height * 0.08,
        margin: EdgeInsets.symmetric(vertical: size.width * 0.05),
        child: FlatButton(
          color: secondary,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BookingScreen(
                    locationName: widget.movieName,
                    locationModel: widget.movie,
                  ),
                ));
          },
          child: Text(
            'Buy Ticket',
            style: TextStyle(
                color: white, fontWeight: FontWeight.bold, fontSize: 18.0),
          ),
        ),
      ),
    );
  }
}
