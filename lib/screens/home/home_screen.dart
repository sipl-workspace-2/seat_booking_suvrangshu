import 'package:flutter/material.dart';
import 'package:seat_booking/constants/constants.dart';
import 'package:seat_booking/screens/home/components/background_list_view.dart';
import 'package:seat_booking/screens/home/components/custom_appbar.dart';
import 'package:seat_booking/screens/home/components/movie_list_view.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Size get size => MediaQuery.of(context).size;

  //to Center the movie List View
  double get locationItemWidth => size.width / 2 + 48;

  ScrollController backgroundScrollController = ScrollController();
  ScrollController locationScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // connecting Background list and Movie list
    locationScrollController.addListener(() {
      backgroundScrollController.jumpTo(
          locationScrollController.offset * (size.width / locationItemWidth));
    });

    return Scaffold(
      backgroundColor: black,
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          //Background of home screen
          BackgroundListView.BackgroundLocationListView(
              backgroundScrollController),

          //Movie detail List view
          LocationListView(
            locationScrollController,
            locationItemWidth,
          ),

          //Appbar
          CustomAppBar(),
        ],
      ),
    );
  }
}
