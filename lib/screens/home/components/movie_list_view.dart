import 'package:flutter/material.dart';
import 'package:seat_booking/data/data.dart';
import 'package:seat_booking/screens/home/components/movie_index.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';

class LocationListView extends StatefulWidget {
  final ScrollController locationScrollControl;
  final double locationItemWidth;

  LocationListView(this.locationScrollControl, this.locationItemWidth);

  @override
  _LocationListViewState createState() => _LocationListViewState();
}

class _LocationListViewState extends State<LocationListView> {
  var locationData = LocationData.LocationData();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return TweenAnimationBuilder(
      duration: Duration(milliseconds: 700),
      tween: Tween<double>(begin: 600, end: 0),
      curve: Curves.easeOutCubic,
      builder: (_, value, child) {
        return Transform.translate(
          offset: Offset(20.0, 0), //vasl
          child: child,
        );
      },
      child: Container(
        height: size.height * .8,
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overScroll) {
            overScroll.disallowIndicator();
            return true;
          },
          child: ScrollSnapList(
            listController: widget.locationScrollControl,
            onItemFocus: (item) {},
            itemSize: widget.locationItemWidth,
            padding: EdgeInsets.zero,
            itemCount: locationData.locationList.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return locationIndex(
                  index, widget.locationScrollControl, widget.locationItemWidth);
            },
          ),
        ),
      ),
    );
  }
}
