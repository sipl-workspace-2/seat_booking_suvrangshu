import 'package:flutter/material.dart';
import 'package:seat_booking/constants/constants.dart';
import 'package:seat_booking/data/data.dart';
import 'package:seat_booking/screens/details/detail_screen.dart';
import 'package:seat_booking/widget/locationFormat.dart';


class locationIndex extends StatefulWidget {
  final int index;
  final ScrollController locationScrollController;
  final double locationItemWidth;

  locationIndex(this.index, this.locationScrollController, this.locationItemWidth);

  @override
  _locationIndexState createState() => _locationIndexState();
}

class _locationIndexState extends State<locationIndex> {
  var locationData = LocationData.LocationData();
  double maxLocationTitle = 65;

  double _locationTitle(double offset, double activeOffset) {
    double translate;
    if (widget.locationScrollController.offset + widget.locationItemWidth <=
        activeOffset) {
      translate = maxLocationTitle;
    } else if (widget.locationScrollController.offset <= activeOffset) {
      translate = maxLocationTitle -
          ((widget.locationScrollController.offset -
                  (activeOffset - widget.locationItemWidth)) /
              widget.locationItemWidth *
              maxLocationTitle);
    } else if (widget.locationScrollController.offset <
        activeOffset + widget.locationItemWidth) {
      translate = ((widget.locationScrollController.offset -
                  (activeOffset - widget.locationItemWidth)) /
              widget.locationItemWidth *
              maxLocationTitle) -
          maxLocationTitle;
    } else {
      translate = maxLocationTitle;
    }
    return translate;
  }

  double _locationDescriptionOpacity(double offset, double activeOffset) {
    double opacity;
    if (widget.locationScrollController.offset + widget.locationItemWidth <=
        activeOffset) {
      opacity = 0;
    } else if (widget.locationScrollController.offset <= activeOffset) {
      opacity = ((widget.locationScrollController.offset -
              (activeOffset - widget.locationItemWidth)) /
          widget.locationItemWidth *
          100);
    } else if (widget.locationScrollController.offset <
        activeOffset + widget.locationItemWidth) {
      opacity = 100 -
          (((widget.locationScrollController.offset -
                      (activeOffset - widget.locationItemWidth)) /
                  widget.locationItemWidth *
                  100) -
              100);
    } else {
      opacity = 0;
    }
    return opacity;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(horizontal: appPadding + 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          AnimatedBuilder(
            animation: widget.locationScrollController,
            builder: (ctx, child) {
              double activeOffset = widget.index * widget.locationItemWidth;

              double translate = _locationTitle(
                  widget.locationScrollController.offset, activeOffset);

              return SizedBox(
                height: translate,
              );
            },
          ),
          SizedBox(
            height: size.height * .008,
          ),
          Container(
            height: size.height * .15,
            child: Align(
              alignment: Alignment.center,
              child: Image(
                width: size.width / 2.5,
                image: locationData.locationList[widget.index].imageLogo.image,
              ),
            ),
          ),
          SizedBox(
            height: size.height * .01,
          ),
          CategoryFormat(locationData.locationList[widget.index].category, white),
          SizedBox(
            height: size.height * .005,
          ),
          Container(
            width: size.width * .25,
            height: 1,
            color: secondary.withOpacity(0.5),
          ),
          SizedBox(
            height: size.height * .01,
          ),
          InkWell(
            onTap: () => Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (context, a1, a2) => DetailScreen(
                          location: locationData.locationList[widget.index],
                          size: size,
                        ))),
            child: Container(
              width: size.width * .25,
              height: size.height * .05,
              decoration: BoxDecoration(
                  color: secondary, borderRadius: BorderRadius.circular(10.0)),
              child: Center(
                  child: Text(
                'BUY TICKET',
                style: TextStyle(
                  color: white,
                  fontWeight: FontWeight.bold,
                ),
              )),
            ),
          ),
          SizedBox(
            height: size.height * .01,
          ),
          GestureDetector(
            onTap: () => Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (context, a1, a2) => DetailScreen(
                          location: locationData.locationList[widget.index],
                          size: size,
                        ))),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(40.0),
              child: Image(
                image: locationData.locationList[widget.index].image.image,
                width: size.width * 0.5,
                height: size.height * 0.35,
              ),
            ),
          ),
          SizedBox(
            height: size.height * .02,
          ),
          AnimatedBuilder(
            animation: widget.locationScrollController,
            builder: (context, child) {
              double activeOffset = widget.index * widget.locationItemWidth;
              double opacity = _locationDescriptionOpacity(
                  widget.locationScrollController.offset, activeOffset);

              return Opacity(
                opacity: opacity / 100,
                child: Column(
                  children: <Widget>[
                    Text(
                      locationData.locationList[widget.index].name,
                      style: TextStyle(
                          color: white,
                          fontSize: size.width / 14,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: size.height * .01,
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
