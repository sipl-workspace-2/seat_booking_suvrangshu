import 'package:flutter/material.dart';
import 'package:seat_booking/models/location_image.dart';
import 'package:seat_booking/models/location_model.dart';
import 'package:seat_booking/models/seat_model.dart';

class LocationData {
  late List<LocationModel> locationList;

  LocationData.LocationData() {
    locationList = List.empty(growable: true);

    locationList.add(
      LocationModel(
        id: 0,
        name: 'Darjeeling',
        category: [
          "Hills",
        ],
        boardingLocation: 'Siliguri',
        describtion:
            'Darjeeling (Bengali: [ˈdarˌdʒiliŋ], Nepali: [darˈd͡ziliŋ]) is a city and municipality in the Eastern Himalayas in India, lying at an elevation of 2,100 metres (7,000 ft) in the northernmost region of the Indian state of West Bengal.[6] It is noted for its tea industry, scenic views of the world\'s third-highest mountain Kangchenjunga, and a narrow-gauge mountain railway, the Darjeeling Himalayan Railway, which is on the UNESCO World Heritage List. Darjeeling is the headquarters of the Darjeeling district which has a partially autonomous status called Gorkhaland Territorial Administration within the state of West Bengal. It is also a popular tourist destination in India.',
        image: Image.asset("assets/image/Darjeeling.jpg"),
        imageLogo: Image.asset("assets/image/DarjeelingLogo.png"),
        locationImageList: [
          LocationImages(
            photo: Image.asset("assets/image/cast/darjeeling-197611__480.jpg"),
          ),
          LocationImages(
            photo: Image.asset("assets/image/cast/himalayas-778418__340.webp"),
          ),
          LocationImages(
            photo: Image.asset("assets/image/cast/toy-train-2725148__340.webp"),
          ),
          LocationImages(
            photo: Image.asset("assets/image/cast/tea-6873722__340.webp"),
          )
        ],
        seats: [
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
        ],
      ),
    );
    locationList.add(
      LocationModel(
        id: 1,
        name: 'Kolkata',
        category: ["City"],
        boardingLocation: 'Siliguri',
        describtion:
            "Kolkata is the capital of the Indian state of West Bengal and was capital of British India until 1912. The city's name was officially changed from Calcutta to Kolkata in January 2001. The urban agglomeration of Kolkata covers several municipal corporations, municipalities, city boards and villages and is the third largest urban agglomeration in India after Mumbai and Delhi. As per the census of 2001, the urban agglomeration's population was 13,216,546 while that of the city was 4,580,544. Kolkata city's population growth has been pretty low in the last decade. The city is situated on the banks of the Hoogli River (a distributary of the Ganges). Some of the renowned engineering marvels associated with Kolkata include the bridges like, Howrah Bridge, Vivekananda Setu and Vidyasagar Setu. Kolkata is the main business, commercial and financial hub of eastern India. The city's economic fortunes turned the tide as the early nineties economic liberalization in India reached Kolkata's shores during late nineties.",
        image: Image.asset("assets/image/kolkata.jpg"),
        imageLogo: Image.asset("assets/image/kolataLogo.webp"),
        locationImageList: [
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/lights-india-west-bengal-kolkata-wallpaper-preview.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/kolkata-howrah-bridge-wallpaper-preview.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/india-temple-kolkata-statue-of-goddess-durga-wallpaper-preview.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/coolpix-hindu-india-kolkata-wallpaper-preview.jpg"),
          ),
        ],
        seats: [
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
        ],
      ),
    );
    locationList.add(
      LocationModel(
        id: 2,
        name: 'Delhi',
        category: ["City"],
        boardingLocation: 'Siliguri',
        describtion:
            "Delhi is a metropolis in northern India. The name Delhi also refers to the National Capital Territory of Delhi (NCT), which is a special union territory jointly administered by the Central government, the NCT elected government and three municipal corporations. The metropolis of Delhi and the National Capital Territory of Delhi are coextensive and for most practical purposes they are considered to be the same entity. New Delhi, an urban area within the metropolis of Delhi, is the seat of the Government of India. Delhi is the sixth most populous metropolis in the world with a population of 15.3 million (2005 figure). Delhi's metropolitan area, informally known as the National Capital Region (NCR), comprises the NCT and the neighbouring satellite towns of Faridabad and Gurgaon in Haryana, and Noida and Ghaziabad in Uttar Pradesh making it the sixth most populous agglomeration in the world, with an estimated population of 19.7 million",
        image: Image.asset("assets/image/delhi.jpg"),
        imageLogo: Image.asset("assets/image/DelhiLogo.jpg"),
        locationImageList: [
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/newdelhi-redfort-landmark-sky-wallpaper-preview.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/street-india-arch-parade-wallpaper-preview.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/india-new-delhi-government-parliament.jpg"),
          ),
          LocationImages(
            photo: Image.asset(
                "assets/image/cast/world-cityscapes-wallpaper-preview.jpg"),
          )
        ],
        seats: [
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: false,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
          SeatModel(
            available: true,
          ),
        ],
      ),
    );
  }
}
